package com.example;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.dao.UseMyBatis;
import com.data.Client;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DemoApplication.class)
@WebAppConfiguration

public class TestJDBC {
  @Autowired
  UseMyBatis useJDBC;

  
  @Test
  public void testJDBC() {
    
    //assertNotNull(useJDBC);
    Client client = useJDBC.getClient(1);
    System.out.println(client.name);
    
  }
  
}
