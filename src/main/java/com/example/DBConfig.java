package com.example;

import java.io.IOException;
import java.io.InputStream;
import javax.sql.DataSource;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.dao.DataBaseDAO;
import com.dao.UseMyBatis;

@Configuration
public class DBConfig {
  
//  @Resource
//  private Environment env;
//   
//  @Bean
//  public DataSource dataSource() {
//          DriverManagerDataSource dataSource = new DriverManagerDataSource();
//          dataSource.setDriverClassName(env.getRequiredProperty("DriverClassName"));
//          dataSource.setUrl(env.getRequiredProperty("URL"));
//          dataSource.setUsername(env.getProperty("name"));
//          dataSource.setPassword(env.getRequiredProperty("Password"));
//          return dataSource;
//  }
  
  @Bean
  public DataBaseDAO dataBaseDAO(){
    return new UseMyBatis();
    //return new UseJDBC();
  }
  
  @Bean 
  public SqlSessionFactory sqlSessionFactory() throws IOException{
    String resource = "mybatis-config.xml";
    InputStream inputStream = Resources.getResourceAsStream(resource);
    return new SqlSessionFactoryBuilder().build(inputStream);
  }
  
}
