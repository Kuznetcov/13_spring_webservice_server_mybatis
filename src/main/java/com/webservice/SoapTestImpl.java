package com.webservice;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dao.DataBaseDAO;
import com.data.Car;
import com.data.Client;
import com.data.Deal;
import com.data.Event;

@WebService(endpointInterface = "com.webservice.SoapTest")
public class SoapTestImpl implements SoapTest {

  @Autowired
  private DataBaseDAO dataBaseDAO;


  @Override
  public String sayHello() {
    return "Hello";
  }

  @Override
  public List<Client> getClient(Integer id) {
    List<Client> list = new ArrayList<>();
    list.add(dataBaseDAO.getClient(id));
    return list;
  }

  @Override
  public String addDeal(Integer dealID, String date, Integer clientID, Integer employeeID,
      Integer parkingPlaceID, Integer carNumber) {
    return dataBaseDAO.addDeal(dealID, date, clientID, employeeID, parkingPlaceID, carNumber);
  }

  @Override
  public List<Deal> getDealsByClientID(Integer clientID) {
    return dataBaseDAO.getDealsByClientID(clientID);
  }

  @Override
  public String addPlaceEvent(Integer eventID, Integer placeID, String date, Event event) {
    return dataBaseDAO.addPlaceEvent(eventID, placeID, date, event);
  }

  @Override
  public List<Deal> getDeal(Deal deal) {
    return dataBaseDAO.getDeal(deal);
  }

}
