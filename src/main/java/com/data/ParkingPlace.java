package com.data;

public class ParkingPlace {

  public Integer placeID;
  public Boolean avaliable=true;
  public Integer owner;
  
  public ParkingPlace(Integer placeID, Boolean avaliable, Integer owner) {
    super();
    this.placeID = placeID;
    this.avaliable = avaliable;
    this.owner = owner;
  }
 
  
  
  
}
