package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.data.Client;

public class ClientMapper implements RowMapper<Client> {

  @Override
  public Client mapRow(ResultSet rs, int arg1) throws SQLException {
    
    Client client = new Client();
    client.setId(rs.getInt("id"));
    client.setName(rs.getString("name"));
    return client;
    
  }

}
