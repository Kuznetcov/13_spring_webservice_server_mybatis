package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.data.Deal;

public class DealMapper implements RowMapper<Deal> {

  @Override
  public Deal mapRow(ResultSet rs, int arg1) throws SQLException {
    
    Deal deal = new Deal();
    deal.setDealID(rs.getInt("id"));
    deal.setDate(rs.getDate("date"));
    deal.setClientID(rs.getInt("client"));
    deal.setEmployeeID(rs.getInt("employee"));
    deal.setParkingPlaceID(rs.getInt("place"));
    deal.setCarNumber(rs.getInt("car"));
    return deal;
  }

}