package com.dao;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.dao.mappers.*;
import com.data.Client;
import com.data.Deal;
import com.data.Event;

public class UseMyBatis implements DataBaseDAO {

  @Autowired
  private SqlSessionFactory sqlSessionFactory;

  @Override
  public Client getClient(Integer id) {

    SqlSession session = sqlSessionFactory.openSession();
    Client client;
    try {
      ClientMapperMyBatis mapper = session.getMapper(ClientMapperMyBatis.class);
      client = mapper.getById(id);
    } finally {
      session.close();
    }
    return client;
  }

  @Override
  public String addDeal(Integer dealID, String date, Integer clientID, Integer employeeID,
      Integer parkingPlaceID, Integer carNumber) {
    DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    LocalDate localDate = LocalDate.parse(date, fmt);
    SqlSession session = sqlSessionFactory.openSession();
    DealMapperMyBatis mapper;
    try {
      mapper = session.getMapper(DealMapperMyBatis.class);
      mapper.addDeal(dealID, localDate, clientID, employeeID, parkingPlaceID, carNumber);
      session.commit();
    } finally {
      session.close();
    }
    return new String("Added " + dealID);

  }

  @Override
  public List<Deal> getDealsByClientID(Integer clientID) {
    SqlSession session = sqlSessionFactory.openSession();
    List<Deal> deals;
    try {
      DealMapperMyBatis mapper = session.getMapper(DealMapperMyBatis.class);
      deals = mapper.getDealsByClientId(clientID);
    } finally {
      session.close();
    }
    return deals;
  }

  @Override
  public String addPlaceEvent(Integer eventID, Integer placeID, String date, Event event) {
    DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    LocalDate localDate = LocalDate.parse(date, fmt);
    SqlSession session = sqlSessionFactory.openSession();

    try {
      PlaceEventsMapperMyBatis mapper = session.getMapper(PlaceEventsMapperMyBatis.class);
      mapper.addPlaceEvent(eventID, placeID, localDate, event.getName());
      session.commit();
    } finally {
      session.close();
    }
    return new String("Added " + eventID);

  }

  @Override
  public List<Deal> getDeal(Deal deal) {
    SqlSession session = sqlSessionFactory.openSession();
    List<Deal> deals;
    try {
      DealMapperMyBatis mapper = session.getMapper(DealMapperMyBatis.class);
      deals = mapper.getDeal(deal);
    } finally {
      session.close();
    }
    return deals;
    
  }

}
