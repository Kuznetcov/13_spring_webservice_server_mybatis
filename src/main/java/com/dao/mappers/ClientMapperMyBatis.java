package com.dao.mappers;

import com.data.Client;

public interface ClientMapperMyBatis {

  Client getById(int id);
  
}
