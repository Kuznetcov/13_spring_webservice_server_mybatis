package com.dao.mappers;

import java.time.LocalDate;

import org.apache.ibatis.annotations.Param;


public interface PlaceEventsMapperMyBatis {

  public void addPlaceEvent(
      @Param("eventID") Integer eventID, 
      @Param("placeID") Integer placeID,
      @Param("date") LocalDate date, 
      @Param("event") String event
      );
}
