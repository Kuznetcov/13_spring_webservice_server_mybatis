package com.dao.mappers;

import java.time.LocalDate;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.data.Deal;

public interface DealMapperMyBatis  {

  public List<Deal> getDealsByClientId(int id);
  
  public List<Deal> getDeal(Deal deal);
  
  public void addDeal(
      @Param("dealID") Integer dealID, 
      @Param("date") LocalDate date, 
      @Param("clientID") Integer clientID, 
      @Param("employeeID") Integer employeeID,
      @Param("parkingPlaceID") Integer parkingPlaceID, 
      @Param("carNumber") Integer carNumber);
}
