package com.dao;


import java.util.List;

import com.data.Client;
import com.data.Deal;
import com.data.Event;

public interface DataBaseDAO {

  public Client getClient(Integer id);
  
  public String addDeal(Integer dealID, String date, Integer clientID, Integer employeeID,
      Integer parkingPlaceID, Integer carNumber);
  
  public List<Deal> getDealsByClientID(Integer clientID);
  
  public String addPlaceEvent(Integer eventID, Integer placeID, String date, Event event);
  
  public List<Deal> getDeal(Deal deal);
}
